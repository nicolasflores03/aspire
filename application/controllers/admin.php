<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index()
	{
		$this->users();
	}


	public function users() {
		$this->load->view('admin/user_list');
	}


	public function users_list() {
		$users_list = array();
		$this->load->model('users_model', '', TRUE);
		$users_list = $this->users_model->users_list();		

		
		output_to_json($this,array("data" => $users_list));
	}

	public function add_user() {
		$success = false;
		$this->load->model('users_model', '', TRUE);
		
		$data = array (
			"email_address" => $this->input->post("email_address"),	
			"password" => sha1($this->input->post("password")),	
			"first_name" => $this->input->post("first_name"),	
			"last_name" => $this->input->post("last_name"),	
			"gender" => $this->input->post("gender"),	
			"newsletter" => $this->input->post("newsletter")
		);

		$success = $this->users_model->add_user($data);	
		
		output_to_json($this,array("success" => $success));
	}

	public function edit_user() {
		$success = false;
		$this->load->model('users_model', '', TRUE);
		$id = $this->input->post("user_id");
		$data = array (
			"user_id" => $this->input->post("user_id"),	
			"email_address" => $this->input->post("email_address"),	
			"password" => sha1($this->input->post("password")),	
			"first_name" => $this->input->post("first_name"),	
			"last_name" => $this->input->post("last_name"),	
			"gender" => $this->input->post("gender"),	
			"newsletter" => $this->input->post("newsletter")
		);
		$success = $this->users_model->edit_user($id, $data);	
		output_to_json($this,array("success" => $success));
	}

	public function delete_user() {
		$success = false;
		$this->load->model('users_model', '', TRUE);
		
		$user_id = $this->input->post("user_id");

		$success = $this->users_model->delete_user($user_id);	
		
		output_to_json($this,array("success" => $success));
	}

	public function get_user() {
		$success = false;
		$this->load->model('users_model', '', TRUE);
		$user_id = $this->input->post("user_id");
		$data = $this->users_model->get_user($user_id);	
		
		output_to_json($this,array("data" => $data));
	}


	


}
