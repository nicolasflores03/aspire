<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function index()
	{
		$this->login_page();
	}


	public function login_page() {
		$this->load->view('Auth/login');
    }
    

    public function login()
	{
		$message = "Not Validate";
		if(trim($this->input->post('email_address')) != "" && trim($this->input->post('password')) != "")
		{
			$email_address = $this->input->post('email_address');
			$password = $this->input->post('password');		
		
			$this->load->model('users_model', '', TRUE);
				$user_information = $this->users_model->login($email_address, $password);
				
				if($user_information != false) 
				{
					$login_admin_data = array( 'login_admin_data' =>
						array (
							'id'  => $user_information->user_id,
							'username'  => $user_information->email_address,
						)
					);					
					$this->session->set_userdata($login_admin_data);

					$message = "Completed";
				}
				else
				{
					$message = "Please enter correct username and password";
				}					
		} 
		else 
		{
			$message = "Please enter correct values";					
		}	
		output_to_json($this, $message);	
	}	

	public function logout()
	{
		if($this->session->userdata('login_admin_data')) {
			$login_user_data = $this->session->userdata('login_admin_data');
		}
	
		$this->session->unset_userdata('login_admin_data');
		redirect('auth');
	}
	
}
