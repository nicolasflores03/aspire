<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Top navbar example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" >

  </head>

  <body style="min-height: 75rem;">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
      <a class="navbar-brand" href="#">Aspire</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Users <span class="sr-only">(current)</span></a>
          </li>

        </ul>

         <div class="pull-right">
            <a class="nav-link" href="<?=base_url();?>/auth/logout">Logout</a>
          </div>
      </div>
    </nav>

    <main role="main" class="container">
    <div class="container">
        <h2>Users Table</h2>
        <p>Using datatable</p> 
        <p><button type="button" id="add_user" class="btn btn-primary">Add User</button> </p>
       
        <table id="example" class="table table-striped table-bordered" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>

                    <th>Email Address</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Gender</th>
                    <th>News Letter</th>
                    <th>Action</th>


                </tr>
            </thead>
        </table>
         
    </main>

    <div class="modal fade" id="show_add_user">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="email_address" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">First Name</label>
                        <input type="text" class="form-control" id="first_name" placeholder="enter fisrt name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Last Name</label>
                        <input type="text" class="form-control" id="last_name" placeholder="enter last name">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect2">Gender</label>
                        <select class="form-control" id="gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                        <input type="checkbox" id="newsletter" class="form-check-input">
                        News Letter
                        </label>
                    </div>
                
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save_user_btn">Add User</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="show_edit_user">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="hidden" id="edit_user_id" >
                        <input type="email" class="form-control" id="edit_email_address" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="edit_password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">First Name</label>
                        <input type="text" class="form-control" id="edit_first_name" placeholder="enter fisrt name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Last Name</label>
                        <input type="text" class="form-control" id="edit_last_name" placeholder="enter last name">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect2">Gender</label>
                        <select class="form-control" id="edit_gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                        <input type="checkbox" id="edit_newsletter" class="form-check-input">
                        News Letter
                        </label>
                    </div>
                
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="edit_user_btn">Save Changers</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url();?>assets/jquery-3.2.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
  </body>
</html>
<script>


 var table

$(document).ready(function(){
    table = $('#example').DataTable( {
        "ajax": "<?=base_url();?>admin/users_list",
        "columns": [
            { "data": "email_address" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "gender" },
            { data: null, render: function ( data, type, row ) {
                if(parseInt(data.newsletter) > 0) {
                    return 'True';
                   
                } else {
                    return 'False';
                }
            }} ,
            { data: null, render: function ( data, type, row ) {
                return '<a onclick="show_edit_user('+data.user_id+'); return false;" title="Edit this row" href="#">Edit</a> | <a onclick="delete_user('+data.user_id+'); return false;" title="Delete this row" href="#" class="">Delete</a>';
            }} 
        ]
    } );

    $('#add_user').click(function(){

        $('#show_add_user').modal('show');
    
    });
        
    $('#save_user_btn').click(function(){

        var newsletter = 0;
        if ($('#newsletter').is(':checked'))
        {
            newsletter = 1;
        }
        var data = {
            email_address: $("#email_address").val(),
            password: $("#password").val(),
            first_name: $("#first_name").val(),
            last_name: $("#last_name").val(),
            gender: $("#gender").find(":selected").val(),
            newsletter: newsletter
        };

       $.post('<?=base_url();?>admin/add_user',data,function(e) {
            $('#show_add_user').modal('toggle');

            table.ajax.reload();
       });
    });

    $('#edit_user_btn').click(function(){

        var newsletter = 0;
        if ($('#edit_newsletter').is(':checked'))
        {
            newsletter = 1;
        }
        var data = {
            user_id: $("#edit_user_id").val(),
            email_address: $("#edit_email_address").val(),
            password: $("#edit_password").val(),
            first_name: $("#edit_first_name").val(),
            last_name: $("#edit_last_name").val(),
            gender: $("#edit_gender").find(":selected").val(),
            newsletter: newsletter
        };

       $.post('<?=base_url();?>admin/edit_user',data,function(e) {
            $('#show_edit_user').modal('toggle');

            table.ajax.reload();
       });
    });
});
function delete_user(id) {

  if(confirm("Are you sure you want to delete this user?") ){
    $.post('<?=base_url();?>admin/delete_user',{user_id: id},function(e) {
        table.ajax.reload();
    });
  }
}
function show_edit_user(id) {

  $.post('<?=base_url();?>admin/get_user',{user_id: id},function(e) {
     $("#edit_user_id").val(e.data.user_id);
     $("#edit_email_address").val(e.data.email_address);
     $("#edit_password").val("fake_password");
     $("#edit_first_name").val(e.data.first_name);
     $("#edit_last_name").val(e.data.last_name);
     $("#edit_gender").val(e.data.gender);

    if(parseInt(e.data.newsletter) > 0) {
        $("#edit_newsletter").prop( "checked", true );
    } else {
        $("#edit_newsletter").prop( "checked", false);
    }
     $('#show_edit_user').modal('show');
  });

}
</script>