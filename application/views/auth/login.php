<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>assets/signin.css" rel="stylesheet">


    <script src="<?=base_url();?>assets/jquery-3.2.1.slim.min.js" ></script>
  </head>

    <form class="form-signin">
      
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="inputEmail" class="form-control"  placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="button" id="btnlogin">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>
</html>
<script src="<?=base_url();?>assets/jquery-3.2.1.slim.min.js" ></script>

<script type="text/javascript">
  var baseurl = "<?php echo base_url();?>";	
    $(document).ready(function(){
      $('#btnlogin').on('click', function(){
        $(this).addClass("disabled");	
        login();		
      });

      $("#password").keypress(function(e) {
          if(e.which == 13) {
            $("#btnlogin").addClass("disabled");	
            login();	
          }
      });
    });	

    function login()
    {
      var email_address = $('#inputEmail').val();
      var password = $('#inputPassword').val();				
      $.ajax({
        type: "POST",
        url: baseurl + "auth/login",
        data: {
            email_address: email_address,
          password: password
        },
        success: function(e){				
          $('#btnlogin').removeClass("disabled");
          if(e != "Completed"){	
            alert(e);
          } else{
            window.location = baseurl + "admin";
          }
        }				
      });
    }
</script>