<?php
	class Users_model extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function login($email_address, $password){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('email_address', $email_address);
			$this->db->where('password', sha1($password));
			$this->db->limit(1);

			$query = $this->db->get();
			
			if($query->num_rows() == 1)
			{
				$result = $query->result();
				return $result[0];
			}
			else
			{
				return false;
			}
		}

		public function users_list(){

			$this->db->select('*');
			$this->db->from('users');
			$this->db->order_by("user_id", "ASC"); 
			return $this->db->get()->result_array(); 
			
		}

		public function get_user($id){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_id', $id);
			$this->db->order_by("user_id", "ASC"); 
			$result = $this->db->get()->result_array(); 
			return $result[0];
		}

		public function add_user($data){
			return $this->db->insert('users', $data); 
		}

		public function edit_user($id, $data){
			
			$this->db->where('user_id', $id); 
			$this->db->update('users', $data); 
			return true;
		}

		public function delete_user($id){
			$this->db->where('user_id', $id);
			$this->db->delete('users'); 
			return true;
		}

	}
?>

