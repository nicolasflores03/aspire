CREATE TABLE `erdb_budget`.`users` (
  `user_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(300) NOT NULL,
  `last_name` VARCHAR(300) NOT NULL,
  `email_address` VARCHAR(300) NOT NULL,
  `password ` VARCHAR(50) NOT NULL,
  `gender` VARCHAR(6) NOT NULL,
  `newsletter` BOOLEAN NOT NULL,
  PRIMARY KEY (`user_id`)
)
ENGINE = InnoDB;